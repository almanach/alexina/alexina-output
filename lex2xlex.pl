#!/usr/bin/env perl

my $all=0;
my $noopt=0;
my $noobl=0;
my $noatt=0;

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-all$/i) {$all=1;}
    elsif (/^-noopt$/i) {$noopt=1;}
    elsif (/^-noobl$/i) {$noobl=1;}
    elsif (/^-noatt$/i) {$noatt=1;}
}

$linenumber=0;
while (<>) {
    if (++$linenumber % 1000 == 0) {print STDERR "Loading lexicon: line $linenumber\r";}
    chomp;
    if (/SeMoyen/) {
      s/(pred=\')([aeiou��])/$1s\'$2/ || s/(pred=\')/$1se /;
      s/^([aeiou��])/s\'$1/ || s/^/se /;
    }
    $orig=$_;
    if (s/,\@Km?s?([ ,>\]])/$1/ && s/,\@active// || $all) {
        s/\].*$/\]/;
	s/(pred=.*?[^_])___[^_]*__[0-9]+/$1/;
	s/:\(([^\)]*)\)/:$1\|__NULL__/g;
	s/,cat=([a-z]+)//;
	$cat=$1;
	s/\]$/,cat=$cat\]/;
	s/>[^,]*\'/>\'/;
	s/([^>])(\',)/\1<>\2/;
	s/\@pron,\@(impers|pers)/\@\1,\@pron/;

	if ($noobl) {
	  s/,(Obl2|Obl|Loc|Dloc):[^,>]*//g;
	}

	if ($noatt) {
	  s/,(Att):[^,>]*//g;
	}

	# on supprime les macros qu'on ne sait pas traiter
	s/\@Ca/\@�a/;
	s/\@[^p�aNIi][^,>\] ]+//g;
	s/\@�a/\@Ca/;
	# on supprime aussi pour le moment les macros qui donnent l'auxiliaire
	s/\@(�tre|avoir),//;

	s/\s*,\s*\@W//g;
	s/\s*;\s*$//;
	push(@lexicon,$_);
    }
    $_=$orig;
    if (/^([^\t]+)\t.*pred=\'([^<]+?)___[^_]*__[0-9]+[<\'].*\@W/) {
	$inf=$1;
	$lemma=$2;
	$lemma2inf{$lemma}=$inf;
    }
}

while ($i<=$#lexicon) {
    if ($lexicon[$i]=~/pred=\'([^<]+)</) {
	$lemma=$1;
	$inf=$lemma2inf{$lemma};
	$lexicon[$i]=~s/^[^\t]+/$inf/;
	$lexicon[$i]=~s/pred=\'([^<]+)</pred=\'$inf</;
    }
    $i++;
}

$linenumber=0;
$i=0;
while ($i<=$#lexicon) {
    @newentries=();
    push(@newentries,$lexicon[$i]);
    $finished=0;
    while (!$finished) {
	$finished=1;
	$j=0;
	while ($j<=$#newentries) {
	    if ($newentries[$j]=~/^(.*pred=[^<]*<[^\|]*:)([^,>]*\|[^,>]*)([,>][^\t]*)/) {
		$finished=0;
		$debut=$1;
		$milieu=$2;
		$fin=$3;
		@tmpentries=();
		for (split(/\|/,$milieu)) {
		    push(@tmpentries,$debut.$_.$fin);
		}
		splice(@newentries,$j,1,@tmpentries);
		$j+=$#newentries;
	    }
	    $j++;
	}
    }
    splice(@lexicon,$i,1,@newentries);
    $i+=$#newentries+1;
}
for (@lexicon) {
  if (!$noopt || $_!~/__NULL__/) {
    s/(?<=[<,])[^:]+:__NULL__//g;
    s/,,+/,/g;
    s/<,+/</;
    s/,+>/>/;
    s/\t[0-9]*\t/ /;
    s/\t//;
    s/^(.*)\@pron_possible(.*)$/\1\2\n\1\@pron\2/;
    print "$_\n";
  }
}
