#!/usr/bin/env perl

use strict;
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

my $entries_number = 0;
my (%lemmas,%reals);
my ($scat,$sf);

while (<>) {
  $entries_number++;
  if ($entries_number % 1000 == 0) {
    print STDERR "$entries_number\r";
  }
  my $line = $_;
  /([^\t]+)\t[^\t]*\t[^\t]*$/;
  $lemmas{$1}++;
  next if ($_!~/pred="[^<]+<([^>]+)>/);
  $scat = $1;
  for (split(/,/,$scat)) {
    /([^:]+):\(?([^\(\)]+)\)?$/ || next;
    $sf = $1;
    for (split(/\|/,$2)) {
      s/__[LD]$//;
      $reals{$sf}{$_}++;
      $reals{__ALL__}{$_}++;
      if ($sf eq "Suj" && (/^cla$/ || /:/ || /^$/)) {print STDERR $line."\n"}
    }
  }
}

print "Entries : $entries_number\n";
print "Lemmas : ".(scalar keys %lemmas)."\n";
for my $sf (sort keys %reals) {
  print "$sf\t".join("|",map {"$_($reals{$sf}{$_})"} sort {$reals{$sf}{$b} <=> $reals{$sf}{$a}} keys %{$reals{$sf}})."\t".(scalar keys %{$reals{$sf}})."\n";
}
