#!/usr/bin/env perl

$o{t}=1; #type
$o{i}=2; #impers/pers
$o{p}=3; #pron

$order=shift;
if ($order =~ /^([tip])([tip])([tip])$/) {
    $o{$1}=1;
    $o{$2}=2;
    $o{$3}=3;
}

while(<>) {
    chomp;
    $t[1]=$t[2]=$t[3]="-";
    /^([^ ]+)\t(.*)/;
    $t[$o{t}]=$1;
    $data=$2;
    if ($data=~/\@(impers|pers)/) {
	$t[$o{i}]=$1;
    }
    if ($data=~/\@(pron)/) {
	$t[$o{p}]="pron";
    }
    for $k1 (" ALL",$t[1]) {
	for $k2 (" ALL",$t[2]) {
	    for $k3 (" ALL",$t[3]) {
		$n{$k1}{$k2}{$k3}++;
	    }
	}
    }
}

$t=0;
$l=0;
for $k1 (sort keys %n) {
    if ($t>0) {$l[0].="\t"};
    $l[0].=$_."|$k1\t|";
    $c=0;
    for $k2 (sort keys %{$n{" ALL"}}) {
	$l[0].="$k2\t|";
	$l=1;
	for $k3 (sort keys %{$n{" ALL"}{" ALL"}}) {
	    if ($t>0 && $c==0) {$l[$l].="\t"};
	    if ($c==0) {$l[$l].="|$k3\t|";}
	    $l[$l].=$n{$k1}{$k2}{$k3}."\t|";
	    $l++;
	}
	$t++;
	$c++;
    }
}

print join("\n",@l)."\n";
