#!/usr/bin/env perl

# usage typique:
# cat *.lex | perl lex2clex.pl lexique.lefff.count > lefff.clex

use strict;

my $countsfile=shift;

open(COUNTS,"<$countsfile") || die "Could not open counts file $countsfile: $!\n";

my @l;
my %countshash_CTFL;
while (<COUNTS>) {
    chomp;
    s/^ +//;
    s/^([0-9]+)./\1\t/;
    @l=split(/ *\t */,$_);
    $l[3]=~s/prorel/prel/g;
    if ($l[3] ne "np") {$l[1]=lc($l[1])}
    if ($l[3] eq "etr") {$l[1]=$l[2]="_ETR"}
    if ($l[2]=~/^[0-9 ,.]+$/) {$l[1]=$l[2]="_NUMBER"}
    $l[4]=~s/^\(null\)$//;
    $countshash_CTFL{$l[3]}{$l[4]}{$l[1]}{$l[2]}+=$l[0];
#    print STDERR "\$countshash_CTFL{$l[3]}{$l[4]}{$l[2]}{$l[1]}=$l[0]\n";
}
close(COUNTS);

my ($lemma,$form,$cat,$tag,$ending);
my %lefffhash_CTFL;
while (<>) {
    chomp;
    $lemma=""; $tag="";
    /^([^\t]+)\t[^\t]*\t([^\t]+)(\t.*)$/;
    $form=$1; $cat=$2; $ending=$3;
    if (/pred *= *\'([^\[]*)\'[<,]/) {$lemma=$1}
    $lemma=~s/__.*//;
    if (/\@([a-zA-Z0-9=,]+)\][^\]]*$/ && $1=~/^([a-zA-Z0-9]+)$/) {$tag=$1}
    $lefffhash_CTFL{$cat}{$tag}{$form}{$lemma}=$cat.$ending;
#    print STDERR "\$lefffhash_CTFL{$cat}{$tag}{$form}{$lemma}=$cat$ending\n";
    if (!defined($countshash_CTFL{$cat}{$tag}{$form}{$lemma})) {
	$countshash_CTFL{$cat}{$tag}{$form}{$lemma}=0;
    }
}

open(USW,">unseen_words") || die "Could not open file unseen_words: $!\n";

for $cat (keys %countshash_CTFL) {
    if (!defined($lefffhash_CTFL{$cat})) {
	print STDERR "WARNING: not such cat $cat in the Lefff\n";
	next;
    } else {
	for $tag (keys %{$countshash_CTFL{$cat}}) {
	    if (!defined($lefffhash_CTFL{$cat}{$tag})) {
		print STDERR "WARNING: not such tag $tag for cat $cat in the Lefff\n";
	    } else {
		for $form (keys %{$countshash_CTFL{$cat}{$tag}}) {
		    if (defined($lefffhash_CTFL{$cat}{$tag}{$form})) {
#			print "> $form : ".defined($lefffhash_CTFL{$cat}{$tag}{$form})." ($cat,$tag)\n";
			for $lemma (keys %{$countshash_CTFL{$cat}{$tag}{$form}}) {
#			    print ">> $lemma\n";
			    if (defined($lefffhash_CTFL{$cat}{$tag}{$form}{$lemma}) && $lefffhash_CTFL{$cat}{$tag}{$form}{$lemma} ne "") {
				print $form."\t".$countshash_CTFL{$cat}{$tag}{$form}{$lemma}."\t".$lefffhash_CTFL{$cat}{$tag}{$form}{$lemma}."\n";
			    } else {
				print USW $form."\t".$cat."\t".$tag."\t".$countshash_CTFL{$cat}{$tag}{$form}{$lemma}."\t".$lemma."\n";
			    }
			}
		    } else {
			print STDERR "WARNING: no such form \"$form\" with cat $cat and tag $tag in the Lefff\n";
			for $lemma (keys %{$countshash_CTFL{$cat}{$tag}{$form}}) {
			    print USW $form."\t".$cat."\t".$tag."\t".$countshash_CTFL{$cat}{$tag}{$form}{$lemma}."\t".$lemma."\n";
			}
		    }
		}
	    }
	}
    }
}
