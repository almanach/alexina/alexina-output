#!/usr/bin/env perl

use CGI qw/:standard Vars/;
use DBI;
use strict;

my %form = Vars();
my $rowmax=$form{rowmax} || 20;
my $rowoffset=$form{rowoffset} || 0;
my $colmax=$form{colmax} || 40;
my $curcat=$form{cat} || "v";
my $regenerate_headers=$form{rh} || 0;
my $filter=$form{filter} || "cat == \"$curcat\"";

my $colwidth=16;

my $pwd;
my $hostname = `hostname`;
chomp($hostname);
if ($hostname eq "graves.inria.fr") {
  $pwd = "/var/www/perl/alexina-interface";
} else {
  $pwd = "/Library/WebServer/perl/alexina-interface";
}

my $dbh = DBI->connect("dbi:SQLite:$pwd/data/intensional_lexicon.dat", "", "",
		       {RaiseError => 1, AutoCommit => 1, RowCacheSize=>1});


my $sth;

my $lemma;
my $saved_something=0;
if ($form{doSave} == 1) {
  for (keys %form) {
    if ($_ eq "newsclasses") {
      for (split(/\//,$form{$_})) {
	if (/^(\@[^=\(]+)\(([^\)]+)\)=(.*)$/) {
	  my $localsclass=$1;
#	  utf8::encode($localsclass);
	  my $localcat=$2;
#	  utf8::encode($localcat);
	  my $localclassdef=$3;
#	  utf8::encode($localclassdef);
	  $sth=$dbh->prepare("insert into scdefs(cat,sclass,data) values(?,?,?) ;\n");
	  $sth->execute($localcat,$localsclass,$localclassdef);
	  $sth=$dbh->prepare("insert into scedefs(cat,sclass,var,data,val,id) values(?,?,?,?,?,?) ;\n");
	  my $a=0;
	  for my $alt_def (split(/ *\| */,$localclassdef)) {
	    $a++;
	    my $n = 0;
	    for my $prop (sort split(/ +/,$alt_def)) {
	      $n++;
	      next if ($prop=~/^\s*$/);
	      $prop=~/^([^=]+)(?:=(.*))?$/;
	      $sth->execute($localcat,$localsclass,$a,$1,$2,$n);
	    }
	  }
	  $saved_something=1;
	}
      }
    }
  }
  if ($saved_something == 1) {
    $sth->finish;
  }
  for (keys %form) {
    if (/^E([0-9]+)$/) {
      my $localsclass=$form{$_};
#      utf8::encode($localsclass);
      $sth=$dbh->prepare("update data set sclass=? where id == ? ;\n");
      $sth->execute($localsclass,$1);
      $saved_something=2;
    }
  }
  if ($saved_something == 2) {
    $sth->finish;
  }
}

my @cats;
$sth=$dbh->prepare(<<STH);
select distinct cat from scdefs;
STH
$sth->execute;
while (my @row = $sth->fetchrow_array) {
#  utf8::decode($row[0]);
  push(@cats,$row[0]);
}

my ($cat,%sprops,%sprop2spropnum);
for $cat (@cats) {
  $sth=$dbh->prepare(<<STH);
select distinct prop from sprops where not ismsprop and prop in (select data from scedefs where cat == ?) order by rank;
STH
  $sth->execute($cat);
  while (my @row = $sth->fetchrow_array) {
#    utf8::decode($row[0]);
    push(@{$sprops{$cat}},$row[0]);
    $sprop2spropnum{$cat}{$row[0]}=$#{$sprops{$cat}};
  }
}
$sth->finish;

my $cmd;
my $localcoloffset;
if ($regenerate_headers || !(-e "$pwd/dyn/col_headers_$curcat\_$colmax\_0_-1.png")) {
  $localcoloffset=0;
  while ($localcoloffset<=$#{$sprops{$curcat}}) {
    my $col_headers;
    my $i;
    for my $col (-1..$colmax-1) {
      last if ($col+$localcoloffset>$#{$sprops{$curcat}});
      if (-e "/usr/bin/convert") {
	$cmd="/usr/bin/convert";
      } elsif (-e "/sw/bin/convert") {
	$cmd="/sw/bin/convert";
      } else {
	die ("/(usr|sw)/bin/convert not found");
      }
      $cmd.=" -size ".(($colmax+5)*$colwidth)."x200 xc:transparent -pointsize 14";
      for $i (0..$colmax-1) {
	last if ($i>$#{$sprops{$curcat}});
	if ($i == $col) {
	  $cmd.=" -undercolor 'LightSkyBlue'";
	}
	$cmd.=" -annotate 295x315+".($colwidth*$i+10)."+200 '".$sprops{$curcat}[$i+$localcoloffset]."'";
	if ($i == $col) {
	  $cmd.=" -undercolor 'transparent'";
	}
      }
      $cmd.=" $pwd/dyn/col_headers_$curcat\_$colmax\_$localcoloffset\_$col.png"; # -trim 
      print STDERR "Generating $pwd/dyn/col_headers_$curcat\_$colmax\_$localcoloffset\_$col.png\n";
      `$cmd`;
    }
    $localcoloffset+=$colmax;
  }
}
$localcoloffset=0;
while ($localcoloffset<=$#{$sprops{$curcat}}) {
  $localcoloffset+=$colmax;
}
my $maxcoloffset = $localcoloffset-$colmax;

my (%sclasses,%sclass2sclassnum);
for $cat (@cats) {
  $sth=$dbh->prepare(<<STH);
select distinct sclass from scdefs where cat == ?;
STH
  $sth->execute($cat);
  while (my @row = $sth->fetchrow_array) {
#    utf8::decode($row[0]);
    push(@{$sclasses{$cat}},$row[0]);
    $sclass2sclassnum{$cat}{$row[0]}=$#{$sclasses{$cat}};
  }
}
$sth->finish;

my (%sclasses_props,%sclasses_propnums);
for $cat (@cats) {
  for my $sclass (@{$sclasses{$cat}}) {
    $sth=$dbh->prepare(<<STH);
select data,var,val from scedefs where cat == ? and sclass == ?;
STH
    $sth->execute($cat,$sclass);
    while (my @row = $sth->fetchrow_array) {
#      utf8::decode($row[0]);
#      utf8::decode($row[2]);
      if ($row[2] eq "") {$row[2]=1}
      $sclasses_props{$cat}{$sclass}[$row[1]-1]{$row[0]}=$row[2];
      $sclasses_propnums{$cat}{$sclass}[$row[1]-1]{$sprop2spropnum{$cat}{$row[0]}}=$row[2];
    }
  }
}
$sth->finish;

if ($filter ne "") {$filter="where (".$filter.")"}
#print STDERR "Filter: $filter\n";
$sth=$dbh->prepare("select cform,mclass,sclass,id from data $filter;\n");
$sth->execute();
my (@lemmas,@lemmasclasses,@lemmasclassesnum,@lemmasid);
while (my @row = $sth->fetchrow_array) {
#  utf8::decode($row[0]);
#  utf8::decode($row[1]);
#  utf8::decode($row[2]);
  push(@lemmas,$row[0]." (".$row[1].")");
  push(@lemmasclasses,$row[2]);  
  push(@lemmasclassesnum,$sclass2sclassnum{$curcat}{$row[2]});  
  push(@lemmasid,$row[3]);
}
$sth->finish;

$rowmax = min($rowoffset+$rowmax-1,$#lemmas)-$rowoffset+1;

print "Content-type: text/html; charset=utf-8\n\n";

print start_html(-title => "Visualisation de la classe", -encoding => "UTF-8");
#, -style => "../styles/viewtable.css"

if ($saved_something) {
  print "(current page successfully saved)<br>\n";
}

# background-color:red; color:white;  font-weight:bold ; width:20px; height:20px
print <<END;
<CENTER>
<SCRIPT type="text/JavaScript">
<!--
var coloffset = 0;
// rowmax=$rowmax;
END

print "var cells=new Array(\n";
for my $row ($rowoffset..$rowoffset+$rowmax-1) {
  if ($row>$rowoffset) {print ",\n"}
  print "new Array(";
  for my $altnum (0..$#{$sclasses_props{$curcat}{$lemmasclasses[$row]}}) {
    if ($altnum>0) {print ",\n"}
    print "new Array(";
    for my $col (0..$#{$sprops{$curcat}}) {
      if ($col>0) {print ","}
      if (defined($sclasses_props{$curcat}{$lemmasclasses[$row]}[$altnum])
	  && $sclasses_props{$curcat}{$lemmasclasses[$row]}[$altnum]{$sprops{$curcat}[$col]}==1) {
	print "1";
      } else {
	print "0";
      }
    }
    print ")";
  }
  print ")";
}
print ");\n";

print "var row2altN=new Array(\n";
my $cumulatedshift=0;
my $altnum;
for my $row ($rowoffset..$rowoffset+$rowmax-1) {
  if ($row>$rowoffset) {print ","}
  print $#{$sclasses_props{$curcat}{$lemmasclasses[$row]}};
}
if ($rowmax==0) {
  print "-1";
}
print ",-1);\n";

print "var row2sclassnum=new Array(\n";
for my $row ($rowoffset..$rowoffset+$rowmax-1) {
  if ($row>$rowoffset) {print ","}
  if ($lemmasclassesnum[$row]>0) {
    print $lemmasclassesnum[$row];
  } else {
    print "0";
  }
}
if ($rowmax==0) {
  print "-1";
}
print ",-1);\n";

print "var row2ssl=new Array(\n"; # ssl=showspecillines
for my $row ($rowoffset..$rowoffset+$rowmax-1) {
  if ($row>$rowoffset) {print ","}
  print "0";
}
if ($rowmax==0) {
  print "-1";
}
print ",-1);\n";

print "var row2lemmaid=new Array(\n";
for my $row ($rowoffset..$rowoffset+$rowmax-1) {
  if ($row>$rowoffset) {print ","}
  print $lemmasid[$row];
}
if ($rowmax==0) {
  print "-1";
}
print ",-1);\n";

print "var sclasses=new Array(\n";
for my $sclassnum (0..$#{$sclasses{$curcat}}) {
  if ($sclassnum>0) {print ",\n"}
  print "new Array(";
  for my $altnum (0..$#{$sclasses_propnums{$curcat}{$sclasses{$curcat}[$sclassnum]}}) {
    for my $spropnum (0..$#{$sprops{$curcat}}) {
      if ($spropnum>0 || $altnum>0) {print ","}
      if ($sclasses_propnums{$curcat}{$sclasses{$curcat}[$sclassnum]}[$altnum]{$spropnum}==1) {
	print "1";
      } else {
	print "0";
      }
    }
  }
  print ")";
}
print ");\n";

print "var sclass2altN=new Array(\n";
for my $sclassnum (0..$#{$sclasses{$curcat}}) {
  if ($sclassnum>0) {print ","}
  print $#{$sclasses_propnums{$curcat}{$sclasses{$curcat}[$sclassnum]}};
}
print ",-1);\n";

print "var sclass2name=new Array(\n";
for my $sclassnum (0..$#{$sclasses{$curcat}}) {
  if ($sclassnum>0) {print ",\n"}
  print "\"".$sclasses{$curcat}[$sclassnum]."\"";
}
print ",-1);\n";

print "var spropnum2spropname=new Array(\n";
for my $spropnum (0..$#{$sprops{$curcat}}) {
  if ($spropnum>0) {print ",\n"}
  print "\"".$sprops{$curcat}[$spropnum]."\"";
}
print ",-1);\n";

print <<END;
var specialcells=new Array();

function load() {
  if (document.images) {
    this.length=load.arguments.length;
    for (var i=0;i<this.length;i++) {
      this[i+1]=new Image();
      this[i+1].src=load.arguments[i];
    }
  }
}

function preload() {
END
print "var temp=new load(\"img/stdplus.png\",\"img/stdminus.png\",\"img/curplus.png\",\"img/curminus.png\",\"img/grey.png\",
\"img/minus.png\",\"img/plus.png\",
\"img/stdplus2.png\",\"img/stdminus2.png\",\"img/curplus2.png\",\"img/curminus2.png\",\"img/grey2.png\",
\"img/minus2.png\",\"img/plus2.png\"";
my $localcoloffset=0;
while ($localcoloffset<=$#{$sprops{$curcat}}) {
  for (-1..$colmax-1) {
    last if ($_+$localcoloffset>$#{$sprops{$curcat}});
    print ",\"/perl/alexina-interface/dyn/col_headers_$curcat\_$colmax\_$localcoloffset\_$_.png\"";
  }
  $localcoloffset+=$colmax;
}
print <<END;
)
}

preload()

function prev() {
  if (coloffset>=$colmax) {
    coloffset-=$colmax;
    redrawData();
  }
}

function next() {
  if (coloffset<$maxcoloffset) {
    coloffset+=$colmax;
    redrawData();
  }
}

function redrawData() {
  document.images["headers"].src = "dyn/col_headers_$curcat\_$colmax\_"+coloffset+"_-1.png";
  for (var r=0;r<$rowmax;r++) {
    document.getElementById("td"+r+"_data").innerHTML = generateData(r);
  }
}

function newalt(r,a) {
    cells[r].push(cells[r][row2altN[r]]);
    for (var a2=row2altN[r];a2>=a;a2--) {
       for (var z=0;z<=$#{$sprops{$curcat}};z++) {
          cells[r][a2+1][z]=cells[r][a2][z];
       }
    }
    row2altN[r]++;
    guessClass(r);
    document.getElementById("td"+r+"_buttons").innerHTML = generateButtons(r);
    document.getElementById("td"+r+"_data").innerHTML = generateData(r);
}

function delalt(r,a) {
  for (var a2=a;a2<row2altN[r];a2++) {
     for (var z=0;z<=$#{$sprops{$curcat}};z++) {
        cells[r][a2][z]=cells[r][a2+1][z];
     }
  }
  cells[r].pop;
  row2altN[r]--;
  guessClass(r);
  document.getElementById("td"+r+"_buttons").innerHTML = generateButtons(r);
  document.getElementById("td"+r+"_data").innerHTML = generateData(r);
}

function guessClass(r) {
    var newsclass=-2;
    var done=0;
    var sclass = 0;
    var z=0;
    while (sclass<sclasses.length && done==0) {
      if (row2altN[r] == sclass2altN[sclass]) {
        newsclass=sclass;
        for (var a=0;a<=sclass2altN[sclass];a++) {
          z=0;
          while (z<=$#{$sprops{$curcat}}) {
            if (cells[r][a][z] != sclasses[sclass][z+($#{$sprops{$curcat}}+1)*a]) {
               newsclass=-2;
            }    
            z++;
          }
        }
        if (newsclass >= 0) {
           done=1;
        }
      }
      sclass++;
    }
    row2sclassnum[r]=newsclass;
    if (newsclass > $#{$sclasses{$curcat}}) {newsclass++}
    document.getElementById("menu"+r).selectedIndex = newsclass+2;
}

function togglevalue(r,a,y) {
    cells[r][a][y]=1-cells[r][a][y];
    setCurImage(r,a,y);
    guessClass(r);
}

function createNewClass(r) {
  var newsclassname=document.getElementById("td"+r+"_scname").value;
  if (newsclassname.charAt(0) != "@") {
    if (newsclassname.charAt(0) == "(") {
      document.getElementById("td"+r+"_scname").value = "(nom invalide)";
      return;
    }
    newsclassname="@"+newsclassname;
  }
  for (var sclass=0;sclass<sclasses.length;sclass++) {
    if (newsclassname == sclass2name[sclass]) {
      document.getElementById("td"+r+"_scname").value = "(déjà utilisé)";
      return;
    }
  }
  sclass2altN.pop();
  sclass2name.pop();
  sclass2altN.push(row2altN[r]);
  sclass2name.push(newsclassname);
  sclass2altN.push(-1);
  sclass2name.push(-1);
  var newsclass=sclasses.length;
  var newsclassprops=new Array();
  var formdata=" ";
  var bool1=0;
  var bool2=0;
  for (var a = 0; a <= row2altN[r]; a++) {
    if (bool1==1) {formdata+=" | "} else {bool1=1}
    for (var y = 0; y <= $#{$sprops{$curcat}}; y++) {
      newsclassprops.push(cells[r][a][y]);
      if (cells[r][a][y]!=0) {
        if (bool2==1) {formdata+=" "} else {bool2=1}
        formdata+=spropnum2spropname[y];
      }
    }
  }
  formdata+=" ";
  if (document.forms[0].newsclasses.value != "") {document.forms[0].newsclasses.value += "/";}
  document.forms[0].newsclasses.value += newsclassname+"($curcat)="+formdata;
  sclasses.push(newsclassprops);
  row2sclassnum[r]=newsclass;
  for (var r=0;r<$rowmax;r++) {
    document.getElementById("td"+r+"_scmenu").innerHTML = generateSCMenu(r);    
  }
}

function togglesclass(r,sclass) {
  if (sclass>=0) {
    row2altN[r]=sclass2altN[sclass];
    row2sclassnum[r]=sclass;
    for (var a = 0; a <= sclass2altN[sclass]; a++) {
      for (var y = 0; y <= $#{$sprops{$curcat}}; y++) {
        cells[r][a][y]=sclasses[sclass][y+($#{$sprops{$curcat}}+1)*a];
      }
    }
    document.getElementById("td"+r+"_buttons").innerHTML = generateButtons(r);
    document.getElementById("td"+r+"_data").innerHTML = generateData(r);
  } else if (sclass == -1) {
    var str = "";
    str += "<input type=\\\"text\\\" id=\\\"td"+r+"_scname\\\">";
    str += "<input type=\\\"button\\\" onclick=\\\"createNewClass("+r+")\\\" value=\\\"OK\\\">";
    str += "<input type=\\\"button\\\" onclick=\\\"document.getElementById('td"+r+"_scmenu').innerHTML = generateSCMenu("+r+")\\\" value=\\\"Cancel\\\">";
    document.getElementById("td"+r+"_scmenu").innerHTML = str;
    document.getElementById("td"+r+"_scname").focus();
  }
}

function choseStdImage(r,a,y) {
  var cellval;
  if (r>=0) {
    cellval = cells[r][a][y];
  } else {
    cellval = specialcells[a][y];
  }
  if (y<=$#{$sprops{$curcat}}) {
    if (cellval==0) {
      if (r % 2 == 0) {
        return "img/stdminus2.png";
      } else {
        return "img/stdminus.png";
      }
    } else {
      if (r % 2 == 0) {
        return "img/stdplus2.png";
      } else {
        return "img/stdplus.png";
      }
    }
  } else {
    if (r % 2 == 0) {
      return "img/grey2.png";
    } else {
      return "img/grey.png";
    }
  }
}

function choseCurImage(r,a,y) {
  if (y<=$#{$sprops{$curcat}}) {
    if (cells[r][a][y]==0) {
      if (r % 2 == 0) {
        return "img/curminus2.png";
      } else {
        return "img/curminus.png";
      }
    } else {
      if (r % 2 == 0) {
        return "img/curplus2.png";
      } else {
        return "img/curplus.png";
      }
    }
  } else {
    if (r % 2 == 0) {
      return "img/grey2.png";
    } else {
      return "img/grey.png";
    }
  }
}

function resetRowHeaderColor(r) {
    if (r % 2 == 0) {
      document.getElementById("td"+r+"_title").style.background = 'lightgray';
    } else {
      document.getElementById("td"+r+"_title").style.background = 'white';
    }
}

function activateRowHeaderColor(r) {
    document.getElementById("td"+r+"_title").style.background = 'lightskyblue';
}

function setStdImage(r,a,y) {
  if (y<=$#{$sprops{$curcat}}) {
    if (cells[r][a][y]==0) {
      if (r % 2 == 0) {
       document.images["im"+r+"\_"+a+"\_"+y].src = "img/stdminus2.png";
      } else {
       document.images["im"+r+"\_"+a+"\_"+y].src = "img/stdminus.png";
      }
    } else {
      if (r % 2 == 0) {
       document.images["im"+r+"\_"+a+"\_"+y].src = "img/stdplus2.png";
      } else {
       document.images["im"+r+"\_"+a+"\_"+y].src = "img/stdplus.png";
      }
    }
    document.images["headers"].src = "dyn/col_headers_$curcat\_$colmax\_"+coloffset+"_-1.png";
    resetRowHeaderColor(r);
  }
}

function setCurImage(r,a,y) {
  if (y<=$#{$sprops{$curcat}}) {
    if (cells[r][a][y]==0) {
      if (r % 2 == 0) {
       document.images["im"+r+"\_"+a+"\_"+y].src = "img/curminus2.png";
      } else {
       document.images["im"+r+"\_"+a+"\_"+y].src = "img/curminus.png";
      }
    } else {
      if (r % 2 == 0) {
       document.images["im"+r+"\_"+a+"\_"+y].src = "img/curplus2.png";
      } else {
       document.images["im"+r+"\_"+a+"\_"+y].src = "img/curplus.png";
      }
    }
    document.images["headers"].src = "dyn/col_headers_$curcat\_$colmax\_"+coloffset+"_"+(y-coloffset)+".png";
    activateRowHeaderColor(r);
  }
}

function generateButtons(r) {
  var result="<TABLE border=0 cellpadding=1 cellspacing=0>";
  for (var a=0;a<=row2altN[r];a++) {
    result += "<TR>";
    result += "<TD align=\\\"right\\\">";
    if (row2altN[r]>0) {
      if (r % 2 == 0) {
        result += "<a href=\\\"javascript:delalt("+r+","+a+")\\\"><image border=\\\"none\\\" id=\\\"del"+r+"\\\" src=\\\"img/minus2.png\\\"/></a>";
      } else {
        result += "<a href=\\\"javascript:delalt("+r+","+a+")\\\"><image border=\\\"none\\\" id=\\\"del"+r+"\\\" src=\\\"img/minus.png\\\"/></a>";
      }
    }
    if (r % 2 == 0) {
      result += "<a href=\\\"javascript:newalt("+r+","+a+")\\\"><image border=\\\"none\\\" id=\\\"add"+r+"\\\" src=\\\"img/plus2.png\\\"/></a></TD>";
      } else {
      result += "<a href=\\\"javascript:newalt("+r+","+a+")\\\"><image border=\\\"none\\\" id=\\\"add"+r+"\\\" src=\\\"img/plus.png\\\"/></a></TD>";
      }
    result += "</TD>";
    result += "</TR>";
  }
  result+="</TABLE>";
  return result;
}


function generateData(r) {
  var result="<TABLE border=0 cellpadding=0 cellspacing=0>";
  result+="<TR><TD><TABLE border=0 cellpadding=0 cellspacing=0>";
  for (var a=0;a<=row2altN[r];a++) {
    result += "<TR>";
    for (var y=coloffset;y<coloffset+$colmax;y++) {
      result+="<TD id=\\\"td"+r+"\_"+a+"\_"+y+"\\\" onmouseover=\\\"setCurImage(\"+r+\",\"+a+\",\"+y+\")\\\" onmouseout=\\\"setStdImage(\"+r+\",\"+a+\",\"+y+\")\\\" bordercolor=\\\"LightGray\\\">";
      if (y<=$#{$sprops{$curcat}}) {
        result += "<a href=\\\"javascript:togglevalue("+r+","+a+","+y+")\\\">";
        result += "<image border=\\\"none\\\" id=\\\"im"+r+"\_"+a+"\_"+y+"\\\" src=\\\"";
        result += choseStdImage(r,a,y);
        result += "\\\">";
        result += "</a></TD>";
      } else {
        if (r % 2 == 0) {
          result += "<image border=\\\"none\\\" id=\\\"im"+r+"\_"+a+"\_"+y+"\\\" src=\\\"img/grey2.png\\\">";
        } else {
          result += "<image border=\\\"none\\\" id=\\\"im"+r+"\_"+a+"\_"+y+"\\\" src=\\\"img/grey.png\\\">";
        }
      }
    }
    result += "</TR>";
  }
  result += "</TABLE></TD></TR>";
  if (row2ssl[r]==1) {
    result+="<TR><TD><TABLE border=0 cellpadding=0 cellspacing=0>";
    document.getElementById("warning").innerHTML += "---";
    for (var a=0;a<specialcells.length;a++) {
      document.getElementById("warning").innerHTML += "+";
      result += "<TR>";
      for (var y=coloffset;y<coloffset+$colmax;y++) {
        document.getElementById("warning").innerHTML += ".";
        result+="<TD id=\\\"tdspec\_"+a+"\_"+y+"\\\" bordercolor=\\\"LightGray\\\">";
        if (y<=$#{$sprops{$curcat}}) {
          result += "<image border=\\\"none\\\" id=\\\"imspec\_"+a+"\_"+y+"\\\" src=\\\"";
          result += choseStdImage(-1,a,y);
          result += "\\\">";
        } else {
          if (a % 2 == 0) {
            result += "<image border=\\\"none\\\" id=\\\"im"+r+"\_"+a+"\_"+y+"\\\" src=\\\"img/grey2.png\\\">";
          } else {
            result += "<image border=\\\"none\\\" id=\\\"im"+r+"\_"+a+"\_"+y+"\\\" src=\\\"img/grey.png\\\">";
          }
        }
      }
      result += "</TR>";
    }
    result += "</TABLE></TD></TR>";
  }
  result+="</TABLE>";
  return result;
}

function generateSCMenu(r) {
  var str = "<SELECT id=\\\"menu"+r+"\\\" name=\\\"E"+row2lemmaid[r]+"\\\"";
  str += " onmouseover=\\\"document.getElementById('td"+r+"_title').style.background = 'lightskyblue'\\\"";
  str += " onmouseout=\\\"resetRowHeaderColor("+r+")\\\"";
  str += " onchange=\\\"togglesclass("+r+",this.selectedIndex-2)\\\"";
  str += ">\\n";
  str += "<OPTION>?</OPTION>\\n";
  str += "<OPTION>...name this class...</OPTION>\\n";
  var selected;
  for (var i=0;i<sclasses.length;i++) {
    if (i == $#{$sclasses{$curcat}}+1) {
      str += "<OPTGROUP label=\\\"Nouvelles classes\\\">\\n";
    }
    if (i == row2sclassnum[r]) {selected = "selected"} else {selected = ""}
    str+="<OPTION "+selected+">"+sclass2name[i]+"</OPTION>\\n";
  }
  if (sclasses.length-1 > $#{$sclasses{$curcat}}) {
    str += "</OPTGROUP>\\n";
  }
  str += "</SELECT>\\n";
  return str;
}

function generateFilterTable (n) {
  for (var i=0;i<=n;i++) {
    return "<tr><td>Ceci est un test</td></tr>";
  }
}

function openFilterWindow () {
  newpage=window.open("filter.pl","Filtre",'width=600,height=250,toolbar=no,scrollbars=no,resizable=no');
}

function changeView (shift) {
  if (shift != 0 && (shift + $rowoffset) >= 0) {
    if (shift > 9999999) {shift = 99999999}
    document.forms[0].rowoffset.value = $rowoffset + shift;
  }
  document.forms[0].doSave.value="0";
  document.forms[0].submit();
}

function computeSpecialCells(r) {
  specialcells = new Array();
  for (var a=0;a<=row2altN[r];a++) {
    specialcells.push(new Array());
    for (var y=coloffset;y<coloffset+$colmax;y++) {
      specialcells[a].push(cells[r][a][y]);
    }  
  }
  document.getElementById("warning").innerHTML = specialcells[0][7]+" - "+row2ssl[r];
}

function showRelatedClasses(r) {
  row2ssl[r]=1-row2ssl[r];
  computeSpecialCells(r);
  document.getElementById("td"+r+"_data").innerHTML = generateData(r);
}

//-->
</SCRIPT>

<FORM action="/perl/alexina-interface/viewtable.pl" method="get">
END

print "<div id=\"warning\"></div>\n";

my $colheader;
print "<TABLE border=0 cellpadding=0 cellspacing=0>\n";
print "<TR>\n";
print "<TD align=\"right\" valign=\"bottom\" id=\"root\"><a href=\"javascript:prev()\"><image border=\\\"none\\\" id=\"prev\" src=\"img/prev.png\"/></a></TD>\n";
print "<TD/>";
print "<TD id=\"tdtitles\" align=\"left\" valign=\"bottom\" colspan=".($colmax+1).">";
print "<SCRIPT type='text/javascript'><!--\n document.write(\"<image border=\\\"none\\\" id=\\\"headers\\\" src=\\\"dyn/col_headers_$curcat\_$colmax\_\"+coloffset+\"_-1.png\\\">\"); \n//--></SCRIPT>\n";
print "<a href=\"javascript:next()\"><image border=\\\"none\\\" id=\"next\" src=\"img/next.png\"/></a></TD>\n";

#print "<TD id=\"tdtitles\" align=\"left\" valign=\"bottom\" colspan=".($colmax+1)."><img src=\"/dyn/col_headers_$curcat.png\"/></TD>\n";

# for my $col (0..$colmax-1) {
#   $colheader=$sprops{$curcat}[$col];
#   $colheader=~s/(?<=[^>])(.)/<br>$1/g;
#   $colheader=~s/_/ /g;
#   print "<TD id=\"tdtitle\_$col\" align=\"center\" valign=\"bottom\">$colheader</TD>\n";
# }

#print "<TD id=\"menus\"></TD>\n";
print "</TR>\n";

my ($altN,$rowtitle,$rowspan);
if ($rowmax == 0) {
  print "<TR>\n";
  print "<TD/>";
  print "<TD align=\"center\" colspan=".($colmax+1)."><i>Aucune entrée trouvée (filtre: $filter)</i></TD>";
  print "<TD/>";
  print "</TR>\n";
} else {
  for my $row (0..$rowmax-1) {
    $rowtitle=$lemmas[$row+$rowoffset];
    #  utf8::encode($rowtitle);
    $altN=$#{$sclasses_props{$curcat}{$lemmasclasses[$row+$rowoffset]}};
    print "<TR ";
    if ($row % 2 == 0) {
      print "bgcolor=\"lightgrey\"";
    } else {
      print "bgcolor=\"white\"";
    }
    print ">\n";
    print "<TD id=\"td$row\_title\" onmouseover=\"activateRowHeaderColor($row)\" onmouseout=\"resetRowHeaderColor($row)\">$rowtitle</TD>\n";
    print "<TD id=\"td$row\_buttons\" onmouseover=\"activateRowHeaderColor($row)\" onmouseout=\"resetRowHeaderColor($row)\">";
    print "<SCRIPT type='text/javascript'><!--\n document.write(generateButtons($row)); \n//--></SCRIPT>\n";
    print "</TD>\n";
    print "<TD id=\"td$row\_data\" onmouseover=\"activateRowHeaderColor($row)\" onmouseout=\"resetRowHeaderColor($row)\">";
    print "<SCRIPT type='text/javascript'><!--\n document.write(generateData($row)); \n//--></SCRIPT>\n";
    print "</TD>\n";
    print "<TD id=\"td$row\_scmenu\">\n";
    print "<SCRIPT type='text/javascript'><!--\n document.write(generateSCMenu($row)); \n//--></SCRIPT>\n";
    print "</TD>\n";
    print "<TD id=\"td$row\_scbutton\">\n";
    print "<input type=\"button\" onclick=\"showRelatedClasses($row)\" value=\"More...\">";
    print "</TD>\n";
    print "</TR>\n";
  }
}

print <<END;
</TABLE>

<INPUT type="button" onclick="changeView(-$rowmax)" value="Page préc.">
<INPUT type="button" onclick="changeView($rowmax)" value="Page suiv.">
À partir de la ligne :<INPUT type="textarea" id="rowoffset" name="rowoffset" value="$rowoffset" onchange="document.getElementById('changeview').focus()">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<INPUT type="button" id="changeview" onclick="changeView(0)" value="Actualiser">
<INPUT type="hidden" id="filter" name="filter" value="">
<INPUT type="hidden" id="cat" name="cat" value="$curcat">
<INPUT type="hidden" id="doSave" name="doSave" value="1">
<INPUT type="hidden" id="newsclasses" name="newsclasses" value="">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<INPUT type="button" onclick="openFilterWindow()" value="Rechercher/Filtrer...">
&nbsp;<INPUT type="submit" value="Enregistrer">
<div type="debug"></div>
</FORM>

</BODY>
</HTML>
END

print end_html;
$sth -> finish;
$dbh -> disconnect;

# sub scmenu {
#   my $row=shift;
#   my $rowtitle=shift;
#   my $scn=shift;
#   my $menuitemname;
#   my $reply .= "<SELECT id=\"menu$row\" name=\"E$lemmasid[$row+$rowoffset]:$rowtitle\"";
#   $reply .= " onmouseover=\"document.getElementById('td$row\_title').style.background = 'lightskyblue'\"";
#   $reply .= " onmouseout=\"resetRowHeaderColor($row)\"";
#   $reply .= " onchange=\"togglesclass($row,this.selectedIndex-2)\"";
#   $reply .= ">\n";
#   $reply.="<OPTION>?</OPTION>\n";
#   $reply.="<OPTION>...name this class...</OPTION>\n";
#   my $selected;
#   for my $i (0..$#{$sclasses{$curcat}}) {
#     if ($i == $scn) {$selected = "selected"} else {$selected = ""}
#     $menuitemname=$sclasses{$curcat}[$i];
#     utf8::encode($menuitemname);
#     $reply.="<OPTION $selected>$menuitemname</OPTION>\n";
#   }
#   $reply.="</SELECT>\n";
#   return $reply;
# }

sub min {
  my $a = shift;
  my $b = shift;
  if ($a < $b) {
    return $a;
  }
  return $b;
}
