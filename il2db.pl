#!/usr/bin/env perl

use strict;
use DBI;
use AppConfig qw/:argcount :expand/;

my $config = AppConfig->new(
			    "reset!" => {DEFAULT => '0' }
			   );
$config->args();

if ($config->reset && -e "intensional_lexicon.dat") {
    print STDERR "  Resetting intensional_lexicon.dat\n";
    `rm intensional_lexicon.dat`;
}

my $dbh = DBI->connect("dbi:SQLite:intensional_lexicon.dat", "", "",
		       {RaiseError => 1, AutoCommit => 0});

# create the base
$dbh->do("CREATE TABLE data (cat,cform,mclass,sclass,comments, UNIQUE(cat,cform,mclass,sclass), PRIMARY KEY(cat,cform,mclass,sclass));");
$dbh->commit;
$dbh->do("CREATE TABLE sprops (prop,oper,arg,rank INTEGER,isobl,ismsprop, PRIMARY KEY(prop,oper,arg));");
$dbh->commit;
$dbh->do("CREATE TABLE scdefs (cat,sclass,data, UNIQUE(sclass), PRIMARY KEY(sclass,data));");
$dbh->commit;
$dbh->do("CREATE TABLE scedefs (cat,sclass,var,data,id INTEGER, UNIQUE(sclass,var,id), PRIMARY KEY(sclass,var,data,id));");
$dbh->commit;

my $sth_data=$dbh->prepare('INSERT INTO data(cat,cform,mclass,sclass,comments) VALUES (?,?,?,?,?)');
my $sth_sprops=$dbh->prepare('INSERT INTO sprops(prop,oper,arg,rank,isobl,ismsprop) VALUES (?,?,?,?,?,?)');
my $sth_scdefs=$dbh->prepare('INSERT INTO scdefs(cat,sclass,data) VALUES (?,?,?)');
my $sth_scedefs=$dbh->prepare('INSERT INTO scedefs(cat,sclass,var,data,id) VALUES (?,?,?,?,?)');

my @bases = ("adj",
		"adv",
		"conj",
		"coord",
		"det",
		"nom",
		"nomp",
		"nompred",
		"ponct",
		"pref-suff",
		"prep",
		"pro",
		"v",
		"v-phd",
		"entnom",
		"avoirC1pC2",
		"avoirN1pC2",
		"vC1pN2",
		"vC1pC2");


open(SPROPS,"<syntax.props") || die "IL file syntax.props could not be opened: $!";
my $status=0;
my %functions;
my ($prop,$op,$arg,$rank,$isobl,$ismsprop);
print STDERR "  Loading syntactic properties...";
while (<SPROPS>) {
  chomp;
  s/\#.*$//;
  s/\/\/.*//;
  next if (/^\s*$/);
  if (/^FUNCTIONS$/) {
    $status=1;
  } elsif ($status == 1 && /^\t([^\s]+)/) {
    $functions{$1}=1;
  } elsif (/^([^\t]+)\t([^\t]+)\t([^\t]*)/) {
    $rank++;
    $prop=$1; $op=$2; $arg=$3;
    $isobl=1;
    if ($prop=~s/^\?//) {$isobl=0}
    $ismsprop=0;
    if ($prop=~/^[A-Z]/) {$ismsprop=1}
    $sth_sprops->execute($prop,$op,$arg,$rank,$isobl,$ismsprop);	
  } else {
    die "Error in syntax.props: line \"$_\" in section FUNCTIONS";
  }
}
$dbh->commit;
print STDERR "done\n";

my $status=0;
my ($save_prev_class,$class,%class_def,%cat_of_class);
for my $base (@bases) {
    print STDERR "\r  Loading description files... $base.p               ";
    open(PFILE,"<$base.p") || die "IL file $base.p could not be opened: $!";
    while (<PFILE>) {
	chomp;
	s/\#.*$//;
	s/\/\/.*//;
	next if (/^\s*$/);
	s/^\s*//; s/\s*$//;
	if (/^(\@\S+)/) {
	  if ($class ne "") {$class_def{$class}.= " "}
	  if ($status > 0) {
	    die "File $base.p, line \"$_\", unexpected new class name";
	  }
	  $class = $1;
	  $cat_of_class{$class}=$base;
	} elsif (/^<\s*(\S+)/) {
	  $class_def{$class}.= " $1";
	} elsif (/^\{$/) {
	  $status++;
	  $class_def{$class}.= " {";
	} elsif (/^\|$/) {
	  $status++;
	  $class_def{$class}.= " |";
	} elsif (/^\}$/) {
	  $status=0;
	  $class_def{$class}.= " }";

	}
    }
    $dbh->commit;
    close(PFILE);
}
for $class (keys %class_def) {
  $sth_scdefs->execute($cat_of_class{$class},$class,$class_def{$class});
}
$dbh->commit;
print STDERR "\r  Loading description files...done                    \n";

print STDERR "  Computing inheritance...";
# extension des définitions des classes (calcul de l'héritage)
$status = 1;
my $iclass;
while ($status) {
  $status = 0;
  for $class (keys %class_def) {
    for $iclass (keys %class_def) {
      if ($class_def{$class}=~s/ $iclass /$class_def{$iclass}/g) { 
	$status++
      }
    }
  }
}
my %alt_def;
my ($n,$a);
for $class (keys %class_def) {
  while ($class_def{$class} =~s/(\{ [^\}]*\} )([^\}\|]+)/$2$1/) {}
  while ($class_def{$class} =~s/([^\{\|]+)( \{ [^\}]*\} )/ZZZ$2/) {
    $iclass=$1;
    while ($class_def{$class} =~s/ZZZ([^\}]*?[\{\|])/$1${iclass}ZZZ/) {}
    $class_def{$class} =~s/ZZZ//;
  }
  $class_def{$class}=~s/^ { //;
  $class_def{$class}=~s/^ } //;
  $a=0;
  for my $alt_def (split(/ +\| +/,$class_def{$class})) {
    $a++;
    ${$alt_def{$class}}[$a]=$alt_def;
    $n = 0;
    for (sort split(/ +/,$alt_def)) {
      $n++;
      next if (/^\s*$/);
      $sth_scedefs->execute($cat_of_class{$class},$class,$a,$_,$n);
    }
  }
}
$dbh->commit;
print STDERR "done\n";

for my $base (@bases) {
    print STDERR "\r  Loading data files... $base.ls              ";
    open(ILFILE,"<$base.ls") || die "IL file $base.ls could not be opened: $!";
    while (<ILFILE>) {
	chomp;
	s/\#.*$//;
	s/\/\/.*//;
	next if (/^\s*$/);
	/^([^\t]*)\t([^\t]*)\t([^\t]*)(?:\t(.*))?$/;
	$sth_data->execute($base,$1,$2,$3,$4);
    }
    $dbh->commit;
    close(ILFILE);
}
print STDERR "\r  Loading data files...done                \n";

$sth_scdefs->finish;
$sth_scedefs->finish;
$sth_sprops->finish;
$sth_data->finish;
$dbh->disconnect;
